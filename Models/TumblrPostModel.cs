﻿using DontPanic.TumblrSharp.Client;
using SocialShareApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{
    public class TumblrPostModel
    {
        
        public Content content { get; set; }
        
        
    }
    public class Content
    {
        public string type { get; set; }
    
        public string url { get; set; }
        public string title { get; set; }

        public string description { get; set; }

        public string author { get; set; }

        public string thumbnail { get; set; }

    }

    public class Result1
    {
        public string? result { get; set; }
        public Status Status { get; set; }

    }
}
