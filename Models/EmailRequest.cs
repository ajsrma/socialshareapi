﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{
    public class EmailRequest
    {
        public string Email { get; set; }
        public int Type { get; set; }
    }
}
