﻿using Newtonsoft.Json;
using SocialShareApi.Models.Auth;


namespace SocialShareApi.Models.Accounts
{
    public class AccountsModel
    {
        public Account[]? Accounts { get; set; }
        public Status Status { get; set; }

    }
    public class Account
    {
        public int Id { get; set; }
        public AccountType Account_Type { get; set; }
        public string Access_Token{ get; set; }

        public string Token_Secret { get; set; }
        
        
        public string Account_Name{ get; set; }

        
        public string Account_Id { get; set; }
    }

    public class AccountType
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

  
}
