﻿using Newtonsoft.Json;
using SocialShareApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Runtime.Intrinsics.X86.Bmi1;

namespace SocialShareApi.Models.Accounts
{
    public class PinterestModal
    {
        public Account? ACCOUNT { get; set; }
        public List<PinterestBoard>? BOARDS { get; set; }

        

    }


    //Below classes are only for desearilizing the boards array
    public class PinterestBoards
    {
        public PinterestBoard?[] BOARDS { get; set; }
        public Status? Status { get; set; }
    }

    public class PinterestBoard
    {
        public int Id { get; set; } 

        [JsonProperty("name")]
        public string Board_Name { get; set; }

        [JsonProperty("id")]
        public string Board_Id { get; set; }

        [JsonProperty("image")]
        public BoardImg Board_Img { get; set; }
        public string Account_Id { get; set; }

        [JsonProperty("url")]
        public string Board_Url { get; set; }
    }

    public class BoardImg
    {
        [JsonProperty("60x60")]
        public Image Image { get; set; }
    }

    public class Image
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }
    }


    public class PinterestToken
    {
        public string ACCESS_TOKEN { get; set; }
        public string TOKEN_TYPE { get; set; }
    }





    public class RootBoard
    {
        [JsonProperty("data")]
        public List<PinterestBoard> boards { get; set; }
    }

    public class PinterestAccount : Account
    {
        [JsonProperty("username")]
        public new string Account_Name { get; set; }

        [JsonProperty("id")]
        public new string Account_Id { get; set; }
    }

    public class PinterestAccountData
    {
        [JsonProperty("data")]
        public PinterestAccount PinterestAccount { get; set; }
    }

}
