﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models.Auth
{
    public class User
    {
        public int User_ID { get; set; }
        public string User_Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public char IsEmailVerified { get; set; }

        public string IsShipVistaUser { get; set; }

        public int FbRemainingPostLimit { get; set; }

        public int TwitterRemainingPostLimit { get; set; }

        public int PinterestRemainingPostLimit { get; set; }
    }
}
