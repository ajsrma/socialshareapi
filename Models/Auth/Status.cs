﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models.Auth
{
    /// <summary>
    /// contains the response status
    /// </summary>
    public class Status
    {
        public int Code { get; set; } 
        public string Message { get; set; }

        public  int? id { get; set; }
        public string? Token { get; set; }
        public User? User_INFO { get; set; }

        public ulong? Post_Id { get; set; }

        public string? FBPostId { get; set; }
    }
}
