﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{
    public class CreateTweetModel
    {
        public TwitterContextModel twitterContextModel { get; set; }
        public TweetModel tweetModel { get; set; }
    }
}
