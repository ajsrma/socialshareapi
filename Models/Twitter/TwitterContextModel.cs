﻿namespace SocialShareApi.Models
{
    public class TwitterContextModel
    {
        public string OAuthToken { get;  set; }
        public string OAuthTokenSecret { get;  set; }
        public string Screen_Name { get;  set; }
        public string UserID { get;  set; }
    }
}