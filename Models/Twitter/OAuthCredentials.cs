﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{
    public class OAuthCredentials
    {
        
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AccessToken { get; set; }
        public string  AccessTokenSecret { get; set; }


    }
    public class Param1
    {
        public string oauth_callback { get; set; }
        public string consumer_key { get; set; }
    }
   
}
