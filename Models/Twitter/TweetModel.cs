﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{
    public class TweetModel
    {
        public string Text { get; set; }
        public string ProductLink { get; set; }

        public string ProductImage { get; set; }

        //either can be set to blob or url
        public string ImageType { get; set; }
        public string Summary { get; set; }
    }
}
