﻿
using SocialShareApi.Models.Accounts;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{

    public class SocialPostsModel
    {
        public SocialPostDetails[]? SocialPostDetails { get; set; }
        public Status Status { get; set; }
    }
    public class SocialPost
    {
        public Int32? id { get; set; }
        public Int32 Product_Id { get; set; }

        public string Account_Id { get; set; }
        public string Post_Id { get; set; }

        
        public DateTime Scheduled_Time { get; set; }

        public DateTime? Posting_Time { get; set; }

        public string? Posting_Notes { get; set; }
        /// <summary>
        /// 1 - posted
        /// 2- failed
        /// 3- scheduled
        /// 0- in progress   // this will be used while posting scheduled ones
        /// </summary>
        public int Status { get; set; } // this is status of post

        
    }

    public class SocialPostDetails
    {
        public Int32 Id { get; set; }
        /// <summary>
        /// 1 - posted
        /// 2- failed
        /// 3- scheduled
        /// 0- in progress   // this will be used while posting scheduled ones
        /// </summary>
        public Int32 Status { get; set; } // this is status of post
        public SocialPost SocialPost { get; set; }
        public Product Product { get; set; }

        public Account Account { get; set; }


    }
}
