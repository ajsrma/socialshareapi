﻿using SocialShareApi.Models.Auth;
using System;
using System.Collections.Generic;

namespace SocialShareApi.Models.Products
{
    public class ProductsModel
    {
        public Product[] Products { get; set; }
        public Status Status { get; set; }
        public long? Success { get; set; }
        public long? Failed { get; set; }
    }
    public class Product
    {
        public Int32? Product_id { get; set; }
       
        public string? Title { get; set; }
       //public Int32 Product_Feed_id { get; set; }
        public string? Description { get; set; }
        public string? Imgurl { get; set; }

        public string? Link { get; set; }

        public string? Price { get; set; }
        public string? Condition { get; set; }
        public string?  Availability { get; set; }

       public string? ExceptionMessage { get; set; }
        public Int32 Feed_id { get; set; }
    }
}
