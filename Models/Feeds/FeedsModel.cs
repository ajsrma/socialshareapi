﻿using Microsoft.AspNetCore.Http;
using SocialShareApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models.Feeds
{
    public class FeedsModel
    {
        public Feed[] Feeds { get; set; }
        public Status Status { get; set; }
      
    }
    public class Feed
    {
        

        public Int32 Feed_id { get; set; }
      
        public string Name { get; set; }

        public string Link { get; set; }

        public string Type { get; set; }
        public string Time { get; set; }
        public bool Val { get; set; }

        public int  Item_Fetched { get; set; }

        public int Item_Failed { get; set; }

        public DateTime Last_Fetch_time { get; set; }
    }
}
