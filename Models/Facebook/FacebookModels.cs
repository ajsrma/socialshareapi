﻿using SocialShareApi.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models.Facebook
{
    public class FacebookModels
    {
        public FacebookPage[] data { get; set; }
    }
    public class FBAccessToken
    {
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }

        public int Expires_IN { get; set; }
    }

    public class FacebookPage
    {
        public string ACCESS_TOKEN { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class CreateFacebookPostModel
    {
        public FacebookPage FacebookPage { get; set; }

        public Product Product { get; set; }
    }

    public class FbPostResponse
    {
        public String Id { get; set; }
    }

}
