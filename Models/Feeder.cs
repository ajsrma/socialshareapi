﻿using Microsoft.AspNetCore.Http;
using SocialShareApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Models
{
    public class Feeder
    {
        public long Success { get; set; }
        public long Failed { get; set; }
        public Status Status { get; set; }
        public IFormFile file { get; set; }
    }
}
