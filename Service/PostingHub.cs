﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialShareApi.Service
{
    public class PostingHub : Hub
    {
        public async Task SendPost(string products)
        {
            await Clients.Caller.SendAsync("ReceiveUpdates", products);
        }
    }
}
