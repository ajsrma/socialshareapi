﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace SocialShareApi.Service
{
    public class EmailClass
    {
        private MailMessage mm;
        private System.Net.Mail.SmtpClient smtp;
        private string USER_NAME = "ajay.sharma@shipvista.com";
        private string PASSWORD = "Toronto1@";
        private string SMTP_HOST = "smtp.zoho.com";

        private string SMTP_PORT = "587";
        public EmailClass()
        {
            mm = new MailMessage();
            smtp = new SmtpClient();
            smtp.Host = SMTP_HOST; //   //"smtp.1and1.com; //
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = USER_NAME;
            NetworkCred.Password = PASSWORD;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = Convert.ToInt32(SMTP_PORT);
        }

        public void SetSender(string fromEmailAddress, string senderName)
        {
            mm.From = new MailAddress(fromEmailAddress, senderName);

        }
        public void SetRecipient(string recipient, bool status)
        {
            if (status)
                mm.To.Clear();
            mm.To.Add(new MailAddress(recipient));

        }
        public void createMailBody(string subject, string body)
        {
            mm.Subject = subject;
            mm.Body = body;
            mm.IsBodyHtml = true;
        }

        public bool sendEmail()
        {
            try
            {
                smtp.Send(mm);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
/// <summary>
/// Summary description for Emailclass
/// </summary>
