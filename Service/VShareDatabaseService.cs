﻿
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using SocialShareApi.Models;
using SocialShareApi.Models.Accounts;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Feeds;
using SocialShareApi.Models.Products;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GoogleServiceAPI.Service
{
    public class VShareDatabaseService
    {
        string ConnectionString;
        
        public VShareDatabaseService()
        {
            // ConnectionString = "Data Source=DESKTOP-D3MI2CA\\MSSQLSERVERAJ;Initial Catalog=Vshare;User ID=sa;Password=sapass123!";
            // _configuration = iconfig;
            ConnectionString = "Data Source=74.208.169.33;Initial Catalog=vsharedb2;User ID=shariousaj2;Password=sajpass@123!";

        }

        public List<Feed> getAllFeeds(int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetFeeds", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@user_id", user_id);
            SqlDataReader myReader;

            List<Feed> feeds = new List<Feed>();



            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                Feed feed = new Feed()
                {
                    Feed_id =Decimal.ToInt32(myReader.GetDecimal(0)),
                    Name = myReader.GetString("Name"),
                    Link = myReader.GetString("Link"),
                    Type = myReader.GetString("Feed_Type"),
                    Time = myReader.GetString("Refresh_Time")

                };
                feeds.Add(feed);
            }
            cn.Close();
            return feeds;
        }


        public List<Feed> getAllFeeds()
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetAllFeeds", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader myReader;

            List<Feed> feeds = new List<Feed>();



            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                Feed feed = new Feed()
                {
                    Feed_id = Decimal.ToInt32(myReader.GetDecimal(0)),
                    Name = myReader.GetString("Name"),
                    Link = myReader.GetString("Link"),
                    Type = myReader.GetString("Feed_Type"),
                    Time = myReader.GetString("Refresh_Time")

                };
                feeds.Add(feed);
            }
            cn.Close();
            return feeds;
        }
        //get all feeds



        //Get Single Feed Params - feed_id and user_id

        public Feed getFeed(int feed_id, int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetFeed", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.AddWithValue("@feed_id", feed_id);
            SqlDataReader myReader;


            Feed feed = new Feed();


            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                feed.Feed_id = Decimal.ToInt32(myReader.GetDecimal(0));
                feed.Name = myReader.GetString("Name");
                feed.Link = myReader.GetString("Link");
                feed.Time = myReader.GetString("Refresh_Time");

               
                
            }
            cn.Close();
            return feed;
        }

        public int AddToFeed(string name, string link, string type, string time, int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_AddFeed", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@link", link);
            cmd.Parameters.AddWithValue("@feed_type",type);
            cmd.Parameters.AddWithValue("@refresh_time", time);

            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.Add("@feed_id", SqlDbType.Int);
            cmd.Parameters["@feed_id"].Direction = ParameterDirection.Output;

            cn.Open();
            cmd.ExecuteNonQuery();
            int feed_id = Convert.ToInt32(cmd.Parameters["@feed_id"].Value);

            cn.Close();
            return feed_id;
        }
        //update feed fetch details
        public bool UpdateFeedFetchDetails(int feed_id,int item_fetched,int item_failed, DateTime last_fetch_time )
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_UpdateFeedFetchDetails", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@feed_id", feed_id);
            cmd.Parameters.AddWithValue("@item_fetched", item_fetched);
            cmd.Parameters.AddWithValue("@item_failed", item_failed);
            cmd.Parameters.AddWithValue("@last_fetch_time", last_fetch_time);
 
            cn.Open();
            cmd.ExecuteNonQuery();
            

            cn.Close();
            return true;
        }

        public bool AddProductFromFeed(int? product_id, string title, string description, string imgurl, string link, string availability, string price, string condition, Int32? feed_id)
        {
           
           
                SqlConnection cn = new SqlConnection(ConnectionString);

                SqlCommand cmd = new SqlCommand("SP_AddProductFromFeed", cn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.AddWithValue("@product_feed_id", product_id);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@description", description);
                cmd.Parameters.AddWithValue("@imgurl", imgurl);
                cmd.Parameters.AddWithValue("@link", link);
                cmd.Parameters.AddWithValue("@availability", availability);
                cmd.Parameters.AddWithValue("@price", price);
                cmd.Parameters.AddWithValue("@condition", condition);
                cmd.Parameters.AddWithValue("@feed_id", feed_id);



                cn.Open();
                cmd.ExecuteNonQuery();


                cn.Close();
                cn.Open();
            // bool IsAddedToDB ;
            //SqlDataReader myReader = cmd.ExecuteReader();
            /*if(   Product_Feed_id.GetType().FullName == "System.Int32" &&
                  feed_id.GetType().FullName == "System.Int32" &&
                  title.GetType().FullName == "System.String" &&
                  description.GetType().FullName == "System.String" &&
                  imgurl.GetType().FullName == "System.String" &&
                  link.GetType().FullName == "System.String" &&
                  price.GetType().FullName == "System.String" &&
                  condition.GetType().FullName == "System.String" &&
                  availability.GetType().FullName == "System.String" )*/
            if (
                  title != "" &&
                  description != "" &&
                  imgurl != "" &&
                  link != "" &&
                  price!= "" &&
                  condition != "" &&
                  availability!= "")
            {
                return true;
            }
            else 
            {
                return false;
            }





        }
        public void AddFailedProduct(int? product_id, string? title, string? description, string? imgurl, string? link, string? availability, string? price, string? condition,int feed_id,string? ExceptionMessage)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);

            SqlCommand cmd = new SqlCommand("SP_AddFailedProduct", cn)
            {
                CommandType = CommandType.StoredProcedure
            };
            if (product_id == null) { product_id = 0; ExceptionMessage = "There is no Id "; }
            if(title == null) { title = "X"; ExceptionMessage = "Please enter the title "; }
            if (description == null) { description = "X"; ExceptionMessage = "There is no description "; }
            if (imgurl == null) { imgurl = "X"; ExceptionMessage = "image url is missing"; }
            if (link == null) { link = "X"; ExceptionMessage = " link is empty"; }
            if (availability == null) { availability = "X"; ExceptionMessage = "You forgot to add the availability "; }
            if (price == null) { price = "X"; ExceptionMessage = "price is missing"; }
            if (condition == null) { condition = "X"; ExceptionMessage = "condition is empty"; }

            cmd.Parameters.AddWithValue("@product_feed_id", product_id);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@imgurl", imgurl);
            cmd.Parameters.AddWithValue("@link", link);
            cmd.Parameters.AddWithValue("@availability", availability);
            cmd.Parameters.AddWithValue("@price", price);
            cmd.Parameters.AddWithValue("@condition", condition);
            cmd.Parameters.AddWithValue("@feed_id", feed_id);
            cmd.Parameters.AddWithValue("@exceptionMessage", ExceptionMessage);
 





             cn.Open();
            cmd.ExecuteNonQuery();


            cn.Close();
        }

        public void DeleteFeed(int feed_id, int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_DeleteFeed", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.AddWithValue("@feed_id",feed_id);


            cn.Open();
            cmd.ExecuteNonQuery();


            cn.Close();

        }

        #region Products MEthods

        public List<Product> getAllProducts(int feed_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetProducts", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@feed_id", feed_id);
            SqlDataReader myReader;

            List<Product> products = new List<Product>();



            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                Product product = new Product()
                {
                    Product_id = Decimal.ToInt32(myReader.GetDecimal(0)),
                    Feed_id = Decimal.ToInt32(myReader.GetDecimal(1)),
                   // Product_Feed_id = Decimal.ToInt32(myReader.GetDecimal(1)),
                    
                    Title = myReader.GetString("title"),
                    
                    Description = myReader.GetString("description"),
                    Imgurl = myReader.GetString("imgurl"),
                    Link = myReader.GetString("link"),
                    Price = myReader.GetString("price"),
                    Condition = myReader.GetString("condition"),
                    Availability = myReader.GetString("availability")
                   
                };

                products.Add(product);
            }
            cn.Close();
            return products;
        }
        public List<Product> getFailedProducts(int? feed_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetFailedProducts", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@feed_id", feed_id);
            SqlDataReader myReader;

            List<Product> products = new List<Product>();



            cn.Open();

            myReader = cmd.ExecuteReader();

          //  Product product=new Product() ;
            while (myReader.Read())
            {
                

                    Product  product = new Product()
                     {

                     Product_id =  Decimal.ToInt32(myReader.GetDecimal(0)),
                    Feed_id =  Decimal.ToInt32(myReader.GetDecimal(1)),
                         

                    Title =myReader.GetString("title"),

                    Description = myReader.GetString("description"),
                    Imgurl = myReader.GetString("imgurl"),
                     Link =myReader.GetString("link"),
                        Availability = myReader.GetString("availability"),
                    Price =myReader.GetString("price"),
                    Condition  = myReader.GetString("condition"),
                    ExceptionMessage= myReader.GetString("exceptionMessage")

                    };


                     products.Add(product);
                 }
                
            
            
            
            cn.Close();
            return products;
        }
        

       
        #endregion


        #region Accounts Methods
        public Account getAccount (int id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetAccount", cn);


            cmd.CommandType = CommandType.StoredProcedure;



            cmd.Parameters.AddWithValue("@id", id);


            SqlDataReader myReader;


            Account account = new Account();


            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                account.Access_Token = myReader.GetString("access_token");
                account.Account_Id = myReader.GetString("account_id");




            }
            cn.Close();
            return account;
        }
        public List<Account> getAllAccounts(int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetAccounts", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@user_id", user_id);
            SqlDataReader myReader;

            List<Account> accounts = new List<Account>();



            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                Account account = new Account()
                {
                    Id = Decimal.ToInt32(myReader.GetDecimal(0)),
                    Account_Type = new AccountType {
                        Id = Decimal.ToInt32(myReader.GetDecimal(1)),
                        Value = myReader.GetString("Name")
                    },
                    Access_Token = myReader.GetString("access_token"),
                    Token_Secret = myReader.GetString("token_secret"),
                    Account_Name = myReader.GetString("account_name"),
                    Account_Id = myReader.GetString("account_id")

                };
                accounts.Add(account);
            }
            cn.Close();
            return accounts;
        }

        //Add account to table - params - account obj and user id

        public void AddToAccounts(Account account, int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_AddAccount", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@user_id", user_id );
            cmd.Parameters.AddWithValue("@account_type", account.Account_Type.Id);
            cmd.Parameters.AddWithValue("@access_token", account.Access_Token);
            cmd.Parameters.AddWithValue("@token_secret", account.Token_Secret ?? "0" );
            cmd.Parameters.AddWithValue("@account_name",account.Account_Name);
            cmd.Parameters.AddWithValue("@account_id", account.Account_Id);


            cn.Open();
            cmd.ExecuteNonQuery();


            cn.Close();

        }

        public void DeleteAccount(int id, int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_DeleteAccount", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.AddWithValue("@id", id);


            cn.Open();
            cmd.ExecuteNonQuery();


            cn.Close();

        }


        #endregion


        #region Pinterest
        //Add boards to DB 
        public void AddPinterestBoard(PinterestBoard pinterestBoard, string accountId)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_AddPinterestBoard", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@board_name", pinterestBoard.Board_Name);
            cmd.Parameters.AddWithValue("@board_id", pinterestBoard.Board_Id);
            cmd.Parameters.AddWithValue("@board_img ", pinterestBoard.Board_Img.Image.Url);
            cmd.Parameters.AddWithValue("@board_url ", pinterestBoard.Board_Url);
            cmd.Parameters.AddWithValue("@account_id",accountId);


            cn.Open();
            cmd.ExecuteNonQuery();


            cn.Close();

        }

        public List<PinterestBoard> getPinterestBoards(string account_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetPinterestBoards", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@account_id", account_id);
            SqlDataReader myReader;

            List<PinterestBoard> boards = new List<PinterestBoard>();



            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                PinterestBoard board = new PinterestBoard()
                {
                    Id = Decimal.ToInt32(myReader.GetDecimal(0)),
                    Board_Name = myReader.GetString("board_name"),
                    Board_Img = new BoardImg { Image = new Image { Url = myReader.GetString("board_img") } },
                    Board_Id = myReader.GetString("board_id"),
                    Board_Url = myReader.GetString("board_url"),
                    Account_Id = account_id
                };
                boards.Add(board);
            }
            cn.Close();
            return boards;
        }


        #endregion


        #region Registration/Login

        public void AddUser(User user)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_AddNewUser", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@user_name",user.User_Name);
            cmd.Parameters.AddWithValue("@user_email", user.Email);
            cmd.Parameters.AddWithValue("@password ", user.Password);
            cmd.Parameters.AddWithValue("@isshipvistauser", user.IsShipVistaUser??"N");


            cn.Open();
            cmd.ExecuteNonQuery();


            cn.Close();

        }



        public bool IsAlreadyRegistered(string email)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_CheckRegistrationStatus", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.Add("@status", SqlDbType.Int);
            cmd.Parameters["@status"].Direction = ParameterDirection.Output;


            cn.Open();
            cmd.ExecuteNonQuery();
            bool status = Convert.ToBoolean(cmd.Parameters["@status"].Value);
            cn.Close();
            return status;

        }
        // This function checks for sucessful login
        public Status CheckLogin(User user)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_Checklogin", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

           
            cmd.Parameters.AddWithValue("@email", user.Email);
            cmd.Parameters.AddWithValue("@password ", user.Password);
            cmd.Parameters.AddWithValue("@isshipvistauser", user.IsShipVistaUser);

            cmd.Parameters.Add("@status", SqlDbType.Int);
            cmd.Parameters.Add("@user_id", SqlDbType.BigInt);
            cmd.Parameters.Add("@user_name", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@isemail_verified", SqlDbType.Char, 1);
            
            cmd.Parameters.Add("@fbpostlimit", SqlDbType.Int);
            cmd.Parameters.Add("@twitterpostlimit", SqlDbType.Int);
            cmd.Parameters.Add("@pinterestpostlimit", SqlDbType.Int);


            cmd.Parameters["@status"].Direction = ParameterDirection.Output;
            cmd.Parameters["@user_id"].Direction = ParameterDirection.Output;
            cmd.Parameters["@user_name"].Direction = ParameterDirection.Output;
            cmd.Parameters["@isemail_verified"].Direction = ParameterDirection.Output;

            //limits
            cmd.Parameters["@fbpostlimit"].Direction = ParameterDirection.Output;
            cmd.Parameters["@twitterpostlimit"].Direction = ParameterDirection.Output;
            cmd.Parameters["@pinterestpostlimit"].Direction = ParameterDirection.Output;


            cn.Open();
            cmd.ExecuteNonQuery();
            int code = Convert.ToInt32(cmd.Parameters["@status"].Value);
            if(code != 0)
            {
                User user1 = new User()
                {
                    User_Name = Convert.ToString(cmd.Parameters["@user_name"].Value),
                    User_ID = Convert.ToInt32(cmd.Parameters["@user_id"].Value),
                    IsEmailVerified = Convert.ToChar(cmd.Parameters["@isemail_verified"].Value),
                    Email = user.Email,

                    FbRemainingPostLimit = Convert.ToInt32(cmd.Parameters["@fbpostlimit"].Value),
                    TwitterRemainingPostLimit = Convert.ToInt32(cmd.Parameters["@twitterpostlimit"].Value),
                    PinterestRemainingPostLimit = Convert.ToInt32(cmd.Parameters["@pinterestpostlimit"].Value)
            };

                Status result = new Status()
                {
                    Code = code,
                    Message = "Login Success",
                    User_INFO = user1
                };
                cn.Close();
                return result;

            }
            else
            {
                Status result = new Status()
                {
                    Code = code,
                    Message = "Bad Crendentials"
                };
                cn.Close();
                return result;
            }

            
        }
        //this function updates the email verification status
        public Status UpdateEmailVerificationStatus(string useremail)
        {
            
            string query = "Update dbo.users set isemail_verified = 'Y' , isactive='Y' where email = @useremail";
           
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@useremail", useremail);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    Status result = new Status()
                    {
                        Code=1,
                        Message = "Verified",
                        User_INFO = new User { Email = useremail}
                    };
                    return result;
                }
                catch (Exception)
                {

                    Status result = new Status()
                    {
                        Code = 0,
                        Message = "error occoured"
                    };
                    return result;
                }
            }
        }


        #endregion


        #region SocialPost
        public int AddToSocialPost(int product_id, string account_Id, string post_id,int status, DateTime scheduledTime)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_AddSocialPost", cn)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@product_id", product_id);
            cmd.Parameters.AddWithValue("@account_id",account_Id);
           
            cmd.Parameters.AddWithValue("@post_id", post_id);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@scheduled_time", scheduledTime);

            cmd.Parameters.Add("@id", SqlDbType.Int);
            cmd.Parameters["@id"].Direction = ParameterDirection.Output;

            cn.Open();
            cmd.ExecuteNonQuery();
            int id = Convert.ToInt16(cmd.Parameters["@id"].Value);

            cn.Close();
            return id;
        }

        public SocialPost GetSocialPost(int id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetSocialPost", cn);


                cmd.CommandType = CommandType.StoredProcedure;
            

           
              cmd.Parameters.AddWithValue("@id", id);


            SqlDataReader myReader;


            SocialPost socialPost = new SocialPost();


            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                socialPost.Post_Id = myReader.GetString("post_id");
                socialPost.Account_Id = myReader.GetString("account_id");
               



            }
            cn.Close();
            return socialPost;
        }

        /// <summary>
        /// Gets posts with respect to user id
        /// </summary>
        /// <param name="user_id">Id of the User</param>
        /// <returns>Array of Social Posts</returns>

        internal List<SocialPostDetails> getAllSocialPosts(int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetSocialPostForUser", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@user_id", user_id);
            SqlDataReader myReader;

            List<SocialPostDetails> socialPosts = new List<SocialPostDetails>();

            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                SocialPostDetails socialPost = new SocialPostDetails()
                {
                    Id = Decimal.ToInt32(myReader.GetDecimal("id")),
                    Status = Decimal.ToInt32(myReader.GetInt16("PostStatus")),
                    SocialPost = new SocialPost
                    {
                       Post_Id= myReader.GetString("postId")
                    },
                    Product = new Product
                    {
                        Product_id = Decimal.ToInt32(myReader.GetDecimal("ProductId")),
                        Title = myReader.GetString("ProductTitle"),
                        Link = myReader.GetString("ProductLink"),
                        Description = myReader.GetString("ProductDesc"),
                        Imgurl = myReader.GetString("ProductImage")

                    },
                    Account = new Account
                    {
                        Account_Id = myReader.GetString("AccountId"),
                       Account_Name = myReader.GetString("AccountName"),
                        Account_Type = new AccountType
                        {
                            Id = Decimal.ToInt32(myReader.GetDecimal("AccountType")),
                            Value = myReader.GetString("AccountNameType")
                        }
                    },
                    
                    
                   
                };

                socialPosts.Add(socialPost);
            }
            cn.Close();
            return socialPosts;
        }

        /// <summary>
        /// gets the all the scheduled social post for given hour
        /// </summary>
        /// <param name="time">represents the max value for scheduled time</param>
        /// <returns>List of Scheduled Posts</returns>

        internal List<SocialPostDetails> GetScheduledSocialPosts(DateTime dateTime)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_GetScheduledPostWithinCurrentHour", cn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@current_time", dateTime);
            SqlDataReader myReader;

            List<SocialPostDetails> socialPosts = new List<SocialPostDetails>();

            cn.Open();

            myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                SocialPostDetails socialPost = new SocialPostDetails()
                {
                    Id = Decimal.ToInt32(myReader.GetDecimal("id")),
                    Status = Decimal.ToInt32(myReader.GetInt16("PostStatus")),
                    Product = new Product
                    {
                        Product_id = Decimal.ToInt32(myReader.GetDecimal("ProductId")),
                        Title = myReader.GetString("ProductTitle"),
                        Link = myReader.GetString("ProductLink"),
                        Description = myReader.GetString("ProductDesc"),
                        Imgurl = myReader.GetString("ProductImage")

                    },
                    Account = new Account
                    {
                        Id = Decimal.ToInt32(myReader.GetDecimal("AccountId")),
                        Account_Name = myReader.GetString("AccountName"),
                        Account_Type = new AccountType
                        {
                            Id = Decimal.ToInt32(myReader.GetDecimal("AccountType")),
                            Value = myReader.GetString("AccountNameType")
                        },
                        Access_Token =  myReader.GetString("AccessToken"),
                        Token_Secret = myReader.GetString("TokenSecret"),
                        Account_Id  = myReader.GetString("AccountNumber")
                    }
                };

                socialPosts.Add(socialPost);
            }
            cn.Close();
            return socialPosts;
        }

        internal Status UpdateSocialPost(Int32 id, string postId, int status, DateTime postingtime,string postingnotes)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_UpdateSocialPost", cn)
            {
                CommandType = CommandType.StoredProcedure
            };
            Status result;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@post_id", postId);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@postingtime", postingtime);
            cmd.Parameters.AddWithValue("@postingnotes", postingnotes);
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
                result = new Status
                {
                    Code = 200,
                    Message = "Success"
                };
            }
            catch (Exception ex)
            {
                result = new Status
                {
                    Code = 223,
                    Message = ex.Message
                };
           
            }
            finally
            {
                cn.Close();
              
            }
            return result;
        }


        internal Status UpdateScheduleForPost(Int32 id, DateTime scheduledtime, int status)
        {
            string query = "update social_posts set status = @status, scheduled_time = @scheduledtime where id = @id";

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@scheduledtime", scheduledtime);
                cmd.Parameters.AddWithValue("@status", status);
                cmd.Parameters.AddWithValue("@id", id);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    Status result = new Status()
                    {
                        Code = 1,
                        Message = "Post Updated",
                       
                    };
                    return result;
                }
                catch (Exception)
                {

                    Status result = new Status()
                    {
                        Code = 0,
                        Message = "error occoured"
                    };
                    return result;
                }
            }
        }
        #endregion


        #region Limits on Social Post/ Accounts

        public Status CheckSocialPostLimitForUser(int user_id)
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_CheckSocialPostLimitForUser", cn)
            {
                CommandType = CommandType.StoredProcedure
            };


            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.Add("@fbpostlimit", SqlDbType.Int);
            cmd.Parameters.Add("@twitterpostlimit", SqlDbType.Int);
            cmd.Parameters.Add("@pinterestpostlimit", SqlDbType.Int);

            //limits
            cmd.Parameters["@fbpostlimit"].Direction = ParameterDirection.Output;
            cmd.Parameters["@twitterpostlimit"].Direction = ParameterDirection.Output;
            cmd.Parameters["@pinterestpostlimit"].Direction = ParameterDirection.Output;


            cn.Open();
            cmd.ExecuteNonQuery();
          
                User user1 = new User()
                {
                 
                    User_ID = user_id,
                    FbRemainingPostLimit = Convert.ToInt32(cmd.Parameters["@fbpostlimit"].Value),
                    TwitterRemainingPostLimit = Convert.ToInt32(cmd.Parameters["@twitterpostlimit"].Value),
                    PinterestRemainingPostLimit = Convert.ToInt32(cmd.Parameters["@pinterestpostlimit"].Value)
                };

                Status result = new Status()
                {
                    Code = 1,
                    Message = "Success",
                    User_INFO = user1
                };
                cn.Close();
                return result;
        }

        #endregion

    }
}