﻿using Newtonsoft.Json;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SocialShareApi.Service
{
    public class FacebookService
    {
        private string BASE_URL = "https://graph.facebook.com/";
        internal Status PostToFacebook(CreateFacebookPostModel cfbm)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    {"access_token",    cfbm.FacebookPage.ACCESS_TOKEN },
                    {"message", cfbm.Product.Title },
                    {"link",    cfbm.Product.Link }
                };
                var content = new FormUrlEncodedContent(values);
                var response = httpClient.PostAsync(BASE_URL + cfbm.FacebookPage.Id + "/feed", content).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var fbpost = JsonConvert.DeserializeObject<FbPostResponse>(result);
                    Models.Auth.Status sucstatus = new Models.Auth.Status()
                    {
                        Code = 200,
                        Message = "Success",
                        FBPostId = fbpost.Id
                        
                    };
                    return sucstatus;
                }
                else
                {
                    Models.Auth.Status errstatus = new Models.Auth.Status()
                    {
                        Code = 1,
                        Message = "Something went wrong",
                        Post_Id = 1
                    };
                    return errstatus;
                }
            }
        }
    }
}
