﻿using LinqToTwitter;
using Microsoft.AspNetCore.Http;
using SocialShareApi.Models;
using SocialShareApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SocialShareApi.Service
{
    public class TwitterService
    {

        public  async Task<Models.Auth.Status> PostToTwitter(CreateTweetModel createTweetModel)
        {
            try
            {
                
                var auth = new SingleUserAuthorizer
                {
                    CredentialStore = new InMemoryCredentialStore
                    {
                        ConsumerKey = "Ggu0eDJXw0V4iDaPYcGPwD2jt",//"KnUK3COcpgbJmZ7sfQBaKCJcY",
                        ConsumerSecret = "VSdXJC7Bt4L1xfDSSUW0samV8av9ENO0grbAVXOIxEEfgSh69k",//"iBhwuxMx2g0W4zbo594Hrn40Jk4WJsg5cw4Ru2gE3lyC40NoNB",
                        OAuthToken = createTweetModel.twitterContextModel.OAuthToken,
                        OAuthTokenSecret = createTweetModel.twitterContextModel.OAuthTokenSecret,
                        UserID = Convert.ToUInt64(createTweetModel.twitterContextModel.UserID),
                        ScreenName = createTweetModel.twitterContextModel.Screen_Name
                    }
                };



                TwitterContext context = new TwitterContext(auth);
                List<ulong> mediaIds = null;
                if (createTweetModel.tweetModel.ProductImage != "undefined") // this means that tweet has no image associated with it
                {
                    var client = new WebClient();
                    byte[] imagebytes;
                    if (createTweetModel.tweetModel.ImageType == "url")
                        imagebytes = client.DownloadData(createTweetModel.tweetModel.ProductImage);
                    else
                        imagebytes = Convert.FromBase64String(createTweetModel.tweetModel.ProductImage);

                    var uploadMedia = await context.UploadMediaAsync(imagebytes, "image/jpg", "tweet_image");

                    mediaIds = new List<ulong> { uploadMedia.MediaID };
                }

                //tweet without images if there is link field supplied

                var status = mediaIds == null || (mediaIds != null && createTweetModel.tweetModel.ProductLink != "undefined") ?
                            //tweet without images
                            await context.TweetAsync(createTweetModel.tweetModel.Text + " " + (createTweetModel.tweetModel.ProductLink == "undefined" ? "" : createTweetModel.tweetModel.ProductLink))
                            :
                            //tweet with images
                            await context.TweetAsync(createTweetModel.tweetModel.Text + " " + (createTweetModel.tweetModel.ProductLink == "undefined" ? "" : createTweetModel.tweetModel.ProductLink), mediaIds);

                if (status.CreatedAt != null)
                {
                    Models.Auth.Status sucstatus = new Models.Auth.Status()
                    {
                        Code = 200,
                        Message = "Success",
                        Post_Id = status.StatusID
                    };
                    return sucstatus;
                }
                else
                {
                    Models.Auth.Status errstatus = new Models.Auth.Status()
                    {
                        Code = 1,
                        Message = "Something went wrong",
                        Post_Id = 1
                    };
                    return errstatus;
                }

            }
            catch (Exception ex)
            {
                Models.Auth.Status status = new Models.Auth.Status()
                {
                    Code = 1,
                    Message = ex.Message,
                    Post_Id = 1

                };
                return status;
            }
        }
    }
}
