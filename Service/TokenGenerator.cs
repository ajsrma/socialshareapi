﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialShareApi.Service
{
    public class TokenGenerator
    {
        private const string Secret = "EB56ED85AC75414AB0200F8E4160169AAC1A986E62529402108C432B58ADB7A5";
       public string getJwtToken(int user_id, int expireMinutes = 120)
        {
           

            var tokenHandler = new JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                
                Subject = new ClaimsIdentity(new[]
                {
                        new Claim(ClaimTypes.Name, Convert.ToString(user_id))
                    }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(symmetricKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);
            return token;
        }

        //never expiring jwt token
        public string getJwtTokenForEmail(string email)
        {


            var tokenHandler = new JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(new[]
                {
                        new Claim(ClaimTypes.Name, Convert.ToString(email))
                    }),

                

                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(symmetricKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);
            return token;
        }
        public static int GetUser(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return 0;

                var symmetricKey = Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);
                var data = principal.Claims.ToArray();
                var user_id = Convert.ToInt32(data[0].Value);
                return user_id;
            }

            catch (Exception)
            {
                return 0;
            }
        }


        public static string GetUserEmail(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);
                var data = principal.Claims.ToArray();
                var user_email = data[0].Value;
                return user_email;
            }

            catch (Exception)
            {
                return null;
            }
        }


    }
}
