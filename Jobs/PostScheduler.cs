﻿using GoogleServiceAPI.Service;
using LinqToTwitter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quartz;
using SocialShareApi.Controllers;
using SocialShareApi.Models;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Facebook;
using SocialShareApi.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;



namespace SocialShareApi.Jobs
{
    public interface IPostScheduler
    {   
        public Task Execute();
    };
    public class PostScheduler : IPostScheduler
    {

       
      
        public async Task Execute()
        {
            Debug.Write("Excecutiing job");
            GetAllPostsForCurrentHour();
            await Task.CompletedTask;
        }

        internal async void GetAllPostsForCurrentHour()
        {
            var db = new VShareDatabaseService();
            //get all the scheduled posts for current hour
            var scheduledPosts = db.GetScheduledSocialPosts(DateTime.Now.AddHours(1));
            var status = new Models.Auth.Status();
            foreach(var post in scheduledPosts)
            {
                
                switch(post.Account.Account_Type.Id) 
                {
                    //facebook
                    case 1:
                        var cfbm = new CreateFacebookPostModel
                        {
                            Product = post.Product,

                            FacebookPage = new FacebookPage
                            {
                                ACCESS_TOKEN = post.Account.Access_Token,
                                Name = post.Account.Account_Name,
                                Id = post.Account.Account_Id
                            }
                        };
                        status = new FacebookService().PostToFacebook(cfbm);
                        break;

                    //twitter
                    case 2:
                        var ctm = new CreateTweetModel
                        {
                            tweetModel = new TweetModel
                            {
                                ImageType = "url",
                                ProductImage = post.Product.Imgurl,
                                ProductLink = post.Product.Link,
                                Summary = post.Product.Description,
                                Text = post.Product.Title
                            },
                            twitterContextModel = new TwitterContextModel
                            {
                                OAuthToken = post.Account.Access_Token,
                                OAuthTokenSecret = post.Account.Token_Secret,
                                Screen_Name = post.Account.Account_Name,
                                UserID = post.Account.Account_Id
                            }
                        };


                        status = await new TwitterService().PostToTwitter(ctm);
                        break;

                    //pinterest
                    case 3:
                        break;

                    default:
                        break;
                }

                if(status.Code == 200)
                {
                 
                    //update status of the post from scheduled to post
                    db.UpdateSocialPost(post.Id, status.FBPostId??Convert.ToString(status.Post_Id), 1, DateTime.Now,status.Message);
                }
                else
                {
                    // update as failed with error
                    db.UpdateSocialPost(post.Id, status.FBPostId, 2, DateTime.Now,status.Message);
                }
                
            }

        }
    }
}
