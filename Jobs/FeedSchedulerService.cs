﻿using GoogleServiceAPI.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Quartz;
using SocialShareApi.Controllers;
using SocialShareApi.Models.Feeds;
using SocialShareApi.Models.Products;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SocialShareApi.Jobs
{
    public interface IFeedSchedulerService
    {
        public Task Execute();
    }
    public class FeedSchedulerService : IFeedSchedulerService
    {
        private Timer _timer;

        private IConfiguration _configuration;
        public string CLIENT_URL { get; }
        public string API_URL { get; set; }
        public FeedSchedulerService(IConfiguration iconfig)
        {
            _configuration = iconfig;
            CLIENT_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL");
            API_URL = _configuration.GetValue<string>("URLS:BASE_URL");
            
        }

        public async Task Execute()
        {
            Debug.Write("Executing feed job");
            var db = new VShareDatabaseService();
            var feeds = db.getAllFeeds();

            foreach (var feed in feeds)
            {
                UpdateFeeds(feed);
            }
            await Task.CompletedTask;
        }

        public void UpdateFeeds(Feed feed)
        {

            Console.WriteLine(feed.Feed_id);
            var fail = 0;
            try
            {
                List<Product> products = new List<Product>();
                XDocument document = XDocument.Load(feed.Link);
                XElement channel = document.Root.Element("channel");
                XNamespace xNamespace = XNamespace.Get("http://base.google.com/ns/1.0");

                foreach (var item in channel.Elements("item"))
                {
                    try
                    {
                        var product_feed_id = item.Element(xNamespace + "id").Value;
                        var title = item.Element(xNamespace + "title").Value;
                        var description = item.Element(xNamespace + "description").Value;
                        var link = item.Element(xNamespace + "link").Value;
                        var image_link = item.Element(xNamespace + "image_link").Value;
                        var availability = item.Element(xNamespace + "availability").Value;
                        var price = item.Element(xNamespace + "sale_price").Value;
                        var condition = item.Element(xNamespace + "condition").Value;
                        Product product = new Product
                        {
                            Product_id = Convert.ToInt32(product_feed_id),
                            Title = title,
                            Description = description,
                            Link = link,
                            Imgurl = image_link,
                            Availability = availability,
                            Price = Regex.Match(price, @"\d+").Value,
                            Condition = condition,
                            Feed_id = feed.Feed_id,
                            //IsAddedToDB = false,

                        };
                        products.Add(product);
                       if(products.Count>100)
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        fail++;
                    }

                }
                var db = new VShareDatabaseService();
                foreach (var item in products)
                {
                   // db.AddProductFromFeed(item.Product_id, item.Title, item.Description, item.Imgurl, item.Link, item.Availability, item.Price, item.Condition, item.Feed_id);
                   // item.IsAddedToDB = true;
                }

                db.UpdateFeedFetchDetails(feed.Feed_id, products.Count, fail, DateTime.Now);

            }
            catch (Exception)
            {

                throw;
            }


        }

    }
}
