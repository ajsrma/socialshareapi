﻿using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Threading.Tasks;

namespace SocialShareApi.Jobs
{
    public class PostSchedulerJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public PostSchedulerJobFactory(IServiceProvider serviceProvider)
        {
            this._serviceProvider = serviceProvider;
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _serviceProvider.GetService(typeof(IJob)) as IJob;
        }

        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            if(disposable!=null)
            {
                disposable.Dispose();
            }
            
        }
    }
}
