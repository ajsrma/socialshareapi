﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Products;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        const int USER_ID = 2000;
        // GET: api/Products
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public ProductsModel Get(int id)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                ProductsModel productModel1 = new ProductsModel()
                {
                    Status = Error
                };
                return productModel1;
            }
            List<Product> products= new VShareDatabaseService().getAllProducts(id);
            ProductsModel productsModel = new ProductsModel()
            {
                Products = products.ToArray()
            };
            return productsModel;
        }

        // POST: api/Products
        [HttpPost]
        public void Post([FromBody] string value)
        {

        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
