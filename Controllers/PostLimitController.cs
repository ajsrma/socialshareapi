﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostLimitController : ControllerBase
    {
        private IConfiguration _configuration;

        public string CLIENT_URL { get; }
        public string API_URL { get; set; }

        public PostLimitController(IConfiguration iconfig)
        {
            _configuration = iconfig;
            CLIENT_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL");
            API_URL = _configuration.GetValue<string>("URLS:BASE_URL");
        }

        // GET: api/PostLimit
        [HttpGet]
        public Status Get()
        {
            var db = new VShareDatabaseService();
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);

            var user_id = TokenGenerator.GetUser(accessToken); 
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                
                return Error;
            }

            Status limits = db.CheckSocialPostLimitForUser(user_id);
            
            return limits;
        }

        // GET: api/PostLimit/5
        [HttpGet("{id}")]
        public int Get(int id)
        {
            var db = new VShareDatabaseService();
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var limit = 0;
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };

                return -1;
            }

            Status limits = db.CheckSocialPostLimitForUser(user_id);
            switch (id)
            {
                case 1:
                    limit = limits.User_INFO.FbRemainingPostLimit;
                    break;
                case 2:
                    limit = limits.User_INFO.TwitterRemainingPostLimit;
                    break;
                case 3:
                    limit = limits.User_INFO.PinterestRemainingPostLimit;
                    break;
                default:
                    break;
            }
            return limit;
        }

        // POST: api/PostLimit
        [HttpPost]
        public void Post([FromBody] string value)
        {
          
        }

        // PUT: api/PostLimit/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
    }
}
