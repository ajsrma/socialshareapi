﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using DontPanic.TumblrSharp;

using DontPanic.TumblrSharp.Client;
using DontPanic.TumblrSharp.OAuth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialShareApi.Models;


namespace SocialShareApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TumblerController : ControllerBase
    {
        private static string CONSUMER_KEY = "ZjdcYYeoGQYggNnIrgmsihnCXAISBii8O7Hs5sCEEWGnHUh9Wk";
        private static string CONSUMER_SECRET = "RGpilEzUbSWwzowqijBLksPHhrPhs8W6ZvDRIa5w8rk1bGNWbp";
        private static string OAUTH_TOKEN = "8paVnORcgIJfGBVsWyQUQwyQA9fgV9WGrOGYi2icKQF05DiPcx";
        private static string OAUTH_TOKEN_SECRET = "t8q8tpu5W2kHwRhcBPE5IGb8eiFUkVIwhft6ic0jRA9TtDTr05";

       
        // GET: api/Tumbler
        [HttpGet]
        public TumblrClient Get()
        {

            

            var p = new HmacSha1HashProvider();
         
            
            OAuthClient oAuthClient = new OAuthClient(p, CONSUMER_KEY, CONSUMER_SECRET);

            TumblrClient tumblrClient = new TumblrClientFactory().Create<TumblrClient>(CONSUMER_KEY, CONSUMER_SECRET, new Token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET));


            /*TODO - for indiviual twitter accesss */
            // var requestToken = oAuthClient.GetRequestTokenAsync("https://localhost:44380/api/tumbler/callback").GetAwaiter().GetResult();
            // var authenticateUrl = "https://www.tumblr.com/oauth/authorize?oauth_token=" + requestToken.Key;
            //tumblrClient = new TumblrClientFactory().Create<TumblrClient>(CONSUMER_KEY, CONSUMER_SECRET, new Token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET));

            return tumblrClient;

           
        }
        
        [HttpGet("callback")]
        public void Get([FromQuery] string oauth_token , [FromQuery] string oauth_verifier )
        {
            var p = new HmacSha1HashProvider();


            OAuthClient oAuthClient = new OAuthClient(p, CONSUMER_KEY, CONSUMER_SECRET);
            var requestToken = oAuthClient.GetRequestTokenAsync("https://localhost:44380/api/tumbler/callback").GetAwaiter().GetResult();
           var url = "https://localhost:44380/api/tumbler?oauth_token=" + oauth_token + "&oauth_verifier=" + oauth_verifier;

           
            Token accessToken = oAuthClient.GetAccessTokenAsync(requestToken, url).GetAwaiter().GetResult();
        }

       

    // POST: api/Tumbler only 
    [HttpPost]
    public long Post([FromBody] TumblrPostModel tumblrPostModel)
    {

           TumblrClient tumblrClient = new TumblrClientFactory().Create<TumblrClient>(CONSUMER_KEY, CONSUMER_SECRET, new Token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET));

           
            try
                {

                var tumblrPost = tumblrPostModel.content;
             
                PostData post = PostData.CreateLink(tumblrPost.url, tumblrPost.title, tumblrPost.description,null, PostCreationState.Published); ;
                
              //  PostData post = PostData.CreatePhoto(photos,tumblrPost.title,tumblrPost.url, null, PostCreationState.Published );
              PostCreationInfo x = tumblrClient.CreatePostAsync("canadianbestseller", post).GetAwaiter().GetResult();
                return x.PostId;
            }
            catch(Exception ex)
            {
                return 0;
            }
           
    }

    }
 }
  


