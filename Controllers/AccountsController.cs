﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialShareApi.Models.Accounts;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        // GET: api/Accounts
        [HttpGet]
        public AccountsModel Get()
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            //
            //

            var user_id = TokenGenerator.GetUser(accessToken);
            if(user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                AccountsModel accountsModel1 = new AccountsModel()
                {
                    Status = Error
                };
                return accountsModel1;
            }
            List<Account> accounts = new VShareDatabaseService().getAllAccounts(user_id);
            Status Success = new Status()
            {
                Code = 200,
                Message = "Success"
            };

            AccountsModel accountsModel = new AccountsModel()
            {
                Accounts = accounts.ToArray(),
                Status = Success
            };

            return accountsModel;
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public Account Get(int id)
        {
            Account account = new VShareDatabaseService().getAccount(id);
            return account;
        }

        // POST: api/Accounts
        [HttpPost]
        public Status Post([FromBody] Account account)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
               
                return Error;
            }
            new VShareDatabaseService().AddToAccounts(account, user_id);
            return new Status() { Code = 200, Message = "Sucesss" };
        }

        // PUT: api/Accounts/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public Status Delete(int id)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };

                return Error;
            }
            new VShareDatabaseService().DeleteAccount(id, user_id);
            return new Status() { Code = 200, Message = "Sucesss" };
        }



    }
}
