﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _configuration;

        public string CLIENT_URL { get; }
        public string API_URL { get; set; }

        public LoginController(IConfiguration iconfig)
        {
            _configuration = iconfig;
            CLIENT_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL");
            API_URL = _configuration.GetValue<string>("URLS:BASE_URL");
        }
        // GET: api/Login
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        [HttpGet("{id}")]
        
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        [HttpPost]
        public Status Post([FromBody] User user_info)
        {
              String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
             var user_id = TokenGenerator.GetUser(accessToken);
            var db = new VShareDatabaseService();
            Status status = db.CheckLogin(user_info);
            

            
            if(status.Code == 0)
            {
                if(user_info.IsShipVistaUser == "Y")
                {
                    //add the user to database and update the status with token
                    db.AddUser(user_info);
                    var emailStatus = db.UpdateEmailVerificationStatus(user_info.Email);
                    
                    if(emailStatus.Code == 1)
                    {
                        status = db.CheckLogin(user_info);

                        if(status.Code == 0)
                        {
                            //return error status
                        }
                        else
                        {
                            status.Token = new Service.TokenGenerator().getJwtToken(status.User_INFO.User_ID);
                        }
                    }

                    
                    
                }
            }
            else
            {
               
                status.Token = new Service.TokenGenerator().getJwtToken(status.User_INFO.User_ID);
                
               
            }


            return status;
        }

        // PUT: api/Login/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
