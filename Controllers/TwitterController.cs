﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using GoogleServiceAPI.Service;
using LinqToTwitter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using SocialShareApi.Models;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class TwitterController : ControllerBase
    {
        private string BASE_URL = "https://api.twitter.com/oauth";
        private string CONSUMER_KEY = "KnUK3COcpgbJmZ7sfQBaKCJcY";
        private string CONSUMER_SECRET = "iBhwuxMx2g0W4zbo594Hrn40Jk4WJsg5cw4Ru2gE3lyC40NoNB";
        private string Redirect_URL;
        private string twitterCallbackUrl;



        private IConfiguration _configuration;

        public TwitterController(IConfiguration iconfig)
        {
            _configuration = iconfig;
            Redirect_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL") + "manageAccounts";
            twitterCallbackUrl = _configuration.GetValue<string>("URLS:BASE_URL");
        }

        // POST: api/twitter/
        [HttpGet]
        public Result1 Get()
        {
            String accessToken = "";
            int user_id;
           /* accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
           
            user_id = TokenGenerator.GetUser(accessToken);*/
            if (Request.Headers["Authorization"].ToString().Length > 0)
            {
                accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
                //ToString().Substring(7);
                user_id = TokenGenerator.GetUser(accessToken);
            }
            else
            {
                user_id = 0;
            }



            if (user_id == 0)
            {
                SocialShareApi.Models.Auth.Status Error = new SocialShareApi.Models.Auth.Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                Result1 accountsModel1 = new Result1()
                {
                    Status = Error
                };
                return accountsModel1;
            }
            var auth = new MvcAuthorizer
            {

                CredentialStore = new SessionStateCredentialStore(HttpContext.Session)
                {
                    ConsumerKey = "KnUK3COcpgbJmZ7sfQBaKCJcY",
                    ConsumerSecret = "iBhwuxMx2g0W4zbo594Hrn40Jk4WJsg5cw4Ru2gE3lyC40NoNB"
                }
            };
            var origin = Request.Headers["origin"];
            UriBuilder uri = new UriBuilder(twitterCallbackUrl + "api/twitter/callback?stoken=" + accessToken +"&source="+origin);


            Uri finalUri = uri.Uri;


            var result = auth.BeginAuthorizationAsync(finalUri).GetAwaiter().GetResult();

            var p = JsonConvert.SerializeObject(result);
            Result1 b = new Result1()
            {

                result = p,
            };

            return b;


            //var auth = new SingleUserAuthorizer
            //{
            //    CredentialStore = new SingleUserInMemoryCredentialStore
            //    {
            //        ConsumerKey = "KnUK3COcpgbJmZ7sfQBaKCJcY",
            //        ConsumerSecret = "iBhwuxMx2g0W4zbo594Hrn40Jk4WJsg5cw4Ru2gE3lyC40NoNB",
            //        AccessToken = "1138482833008275459-FUTSHmFpgKQac3nxPCH2MufqLEv2nb",
            //        AccessTokenSecret = "ZIJraTu2z0BFBXNdfT2ATDhIBclBLypK78gNr5fbNrDI4"
            //    }
            //};

            //TwitterContextModel twitterContextModel = new TwitterContextModel()
            //{
            //    OAuthToken = auth.CredentialStore.OAuthToken,
            //    OAuthTokenSecret = auth.CredentialStore.OAuthTokenSecret,
            //    UserID = auth.CredentialStore.UserID,
            //    Screen_Name = auth.CredentialStore.ScreenName

            //};

            //return twitterContextModel;

        }
        [HttpGet("{id}")]
       // [Route("callback")]
        public RedirectResult Get([FromQuery] string oauth_token, [FromQuery] string stoken, [FromQuery] string source)
        {
            String accessToken = stoken;

            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Models.Auth.Status Error = new SocialShareApi.Models.Auth.Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                Result1 accountsModel1 = new Result1()
                {
                    Status = Error
                };
                return Redirect(source+"/manageAccounts"+"?status=error");
            }
            var auth = new MvcAuthorizer
            {
                CredentialStore = new SessionStateCredentialStore(HttpContext.Session)
                {
                    ConsumerKey = "KnUK3COcpgbJmZ7sfQBaKCJcY",
                    ConsumerSecret = "iBhwuxMx2g0W4zbo594Hrn40Jk4WJsg5cw4Ru2gE3lyC40NoNB",
                    OAuthToken = oauth_token
                }
            };
            UriBuilder uri = new UriBuilder(Request.Host.Value + Request.PathBase + Request.Path + Request.QueryString);


            Uri finalUri = uri.Uri;
            auth.CompleteAuthorizeAsync(finalUri).GetAwaiter().GetResult();

            // This is how you access credentials after authorization.
            // The oauthToken and oauthTokenSecret do not expire.
            // You can use the userID to associate the credentials with the user.
            // You can save credentials any way you want - database, 
            //   isolated storage, etc. - it's up to you.
            // You can retrieve and load all 4 credentials on subsequent 
            //   queries to avoid the need to re-authorize.
            // When you've loaded all 4 credentials, LINQ to Twitter will let 
            //   you make queries without re-authorizing.
            //
            var credentials = auth.CredentialStore;
            string oauthToken = credentials.OAuthToken;
            string oauthTokenSecret = credentials.OAuthTokenSecret;
            string screenName = credentials.ScreenName;
            string userID = credentials.UserID.ToString();
            //
            Models.Accounts.AccountType accountType = new Models.Accounts.AccountType()
            {
                Id = 2,
                Value = "Twitter"
            };

            Models.Accounts.Account account = new Models.Accounts.Account()
            {
                Access_Token = oauthToken,
                Token_Secret = oauthTokenSecret,
                Account_Name = screenName,
                Account_Id = userID,
                Account_Type = accountType 
            };
            
            new VShareDatabaseService().AddToAccounts(account, user_id);
           return Redirect(source+"/manageAccounts?status=success");
        }


        [HttpPost]
        public async Task<Models.Auth.Status> Post([FromBody] CreateTweetModel createTweetModel)
        {

            return await new TwitterService().PostToTwitter(createTweetModel);

        }


        



       

    }
}
