﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SocialShareApi.Models.Facebook;

namespace SocialShareApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class FacebookController : ControllerBase
    {
        private string APP_ID = "398078220903968";
        private string APP_SECRET = "42ea3949a2edf6cd9542307b36e616a7";
        // GET: api/Facebook
        [HttpGet]
        public FacebookModels Get([FromQuery] string Access_Token, [FromQuery] string User_ID)
        {
            string  result="";
            using (HttpClient client = new HttpClient())
            {


                try
                {
                    HttpResponseMessage response1 = client.GetAsync("https://graph.facebook.com/v10.0/oauth/access_token?"+
                                      "grant_type=fb_exchange_token"+
                                        "&client_id="+APP_ID+
                                        "&client_secret="+APP_SECRET+
                                        "&fb_exchange_token=" + Access_Token).GetAwaiter().GetResult();

                 var result1 = response1.Content.ReadAsStringAsync().Result;

                 FBAccessToken fBAccessToken = JsonConvert.DeserializeObject<FBAccessToken>(result1);
                    HttpResponseMessage response = client.GetAsync("https://graph.facebook.com/v10.0/"+User_ID+"/accounts?"+
                            "access_token=" + fBAccessToken.Access_Token).GetAwaiter().GetResult();
                   
                result = response.Content.ReadAsStringAsync().Result;

                }
                catch (HttpRequestException e)
                {

                }
                List<FacebookPage> facebookPages = new List<FacebookPage>();

               

            }
            FacebookModels facebookModels = JsonConvert.DeserializeObject<FacebookModels>(result);
            return facebookModels;


        }

        // GET: api/Facebook/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Facebook
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Facebook/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
    }
}
