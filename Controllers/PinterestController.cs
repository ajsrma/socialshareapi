﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SocialShareApi.Models;
using SocialShareApi.Models.Accounts;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class PinterestController : ControllerBase
    {
        private string BASE_URL = "https://api.pinterest.com/oauth/";
        private string CONSUMER_KEY = "5076419457509799538";
        private string CONSUMER_SECRET = "0d68f1fae4b2aaad69e0a5115ecdf285b3b7d83b9305afc1ed687f454b818929";
        private string Redirect_URL;
        private string pinterestCallbackUrl;

        private IConfiguration _configuration;

        public PinterestController(IConfiguration iconfig)
        {
            _configuration = iconfig;
            Redirect_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL") + "manageAccounts";
            pinterestCallbackUrl = _configuration.GetValue<string>("URLS:BASE_URL")+"api/pinterest/callback";
        }



        // GET: api/Pinterest
        [HttpGet]

        public Result1 Get()
        {
           /*string accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            
            var user_id = TokenGenerator.GetUser(accessToken);*/
             String accessToken = "";
             int user_id;
             if (Request.Headers["Authorization"].ToString().Length > 0)
             {
                 accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
                 //ToString().Substring(7);
                 user_id = TokenGenerator.GetUser(accessToken);
             }
             else
             {
                 user_id = 0;
             }



            if (user_id == 0)
            {
                SocialShareApi.Models.Auth.Status Error = new SocialShareApi.Models.Auth.Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                Result1 accountsModel1 = new Result1()
                {
                    Status = Error
                };
                return accountsModel1;
            }

            UriBuilder uri = new UriBuilder(BASE_URL + "?response_type=code&redirect_uri=" + pinterestCallbackUrl + "&client_id=" + CONSUMER_KEY + "&scope=read_public,write_public &state=" + accessToken);


            Uri finalUri = uri.Uri;
            Result1 b = new Result1()
            {

                result = finalUri.ToString()
            };

            return b;
        }

       [HttpGet]
        [Route("callback")]
      
        public RedirectResult Get([FromQuery] string code, [FromQuery] string state)
        {
            var status = "error";
            int user_id = TokenGenerator.GetUser(state);
            string url = "https://api.pinterest.com/v1/oauth/token";
            var values = new Dictionary<string, string>
                            {
                            { "grant_type", "authorization_code" },
                            { "client_id", CONSUMER_KEY },
                            { "client_secret", CONSUMER_SECRET },
                            { "code", code }
                            };


            using (HttpClient httpClient = new HttpClient())
            {
                var content = new FormUrlEncodedContent(values);
                var response = httpClient.PostAsync(url, content).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    PinterestToken pinterestToken = JsonConvert.DeserializeObject<PinterestToken>(result);
                    var response2 = httpClient.GetAsync("https://api.pinterest.com/v1/me/?access_token=" + pinterestToken.ACCESS_TOKEN + "&fields=id%2Cusername").GetAwaiter().GetResult();
                    var boards = httpClient.GetAsync("https://api.pinterest.com/v1/me/boards/?access_token=" + pinterestToken.ACCESS_TOKEN + "&fields=id%2Cimage%2Cname%2Curl").GetAwaiter().GetResult();
                    var boardResponse = boards.Content.ReadAsStringAsync().Result;
                    var accountResponse = response2.Content.ReadAsStringAsync().Result;
                    var pinterestAccountData = JsonConvert.DeserializeObject<PinterestAccountData>(accountResponse);

                    RootBoard rootBoard = JsonConvert.DeserializeObject<RootBoard>(boardResponse);
                    PinterestModal pinterestModal = new PinterestModal()
                    {
                        ACCOUNT = pinterestAccountData.PinterestAccount,
                        BOARDS = rootBoard.boards
                    };
                    AccountType accountType = new AccountType()
                    {
                        Id = 3,
                        Value = "Pinterest"
                    };
                    pinterestModal.ACCOUNT.Access_Token = pinterestToken.ACCESS_TOKEN;
                    pinterestModal.ACCOUNT.Account_Id = pinterestAccountData.PinterestAccount.Account_Id;
                    pinterestModal.ACCOUNT.Account_Name = pinterestAccountData.PinterestAccount.Account_Name;
                    pinterestModal.ACCOUNT.Account_Type = accountType;

                    new VShareDatabaseService().AddToAccounts(pinterestModal.ACCOUNT, user_id);
                    pinterestModal.BOARDS.ForEach(board => new VShareDatabaseService().AddPinterestBoard(board, pinterestModal.ACCOUNT.Account_Id));
                    status = "success";
                }


            }
            return Redirect(Redirect_URL + "?status=" + status);
        }

        

        // GET: api/Pinterest/5
        [HttpGet("{id}", Name = "Get")]
        
        public PinterestBoards Get(string id)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                PinterestBoards accountsModel1 = new PinterestBoards()
                {
                    Status = Error
                };
                return accountsModel1;
            }

            List<PinterestBoard> boards = new VShareDatabaseService().getPinterestBoards(id);
            PinterestBoards pinterestBoards = new PinterestBoards
            {
                BOARDS = boards.ToArray()
            };
            return pinterestBoards;
        }

        // POST: api/Pinterest
        [HttpPost]
        public void Post([FromBody] PinterestBoard pinterestBoard)
        {
            //new VShareDatabaseService().AddPinterestBoard(pinterestBoard);
        }

        // PUT: api/Pinterest/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
