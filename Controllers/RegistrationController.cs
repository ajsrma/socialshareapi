﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private IConfiguration _configuration;

        public string CLIENT_URL { get; }
        public string API_URL { get; set; }

        public RegistrationController(IConfiguration iconfig)
        {
            _configuration = iconfig;
            CLIENT_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL");
            API_URL = _configuration.GetValue<string>("URLS:BASE_URL");
        }
        // GET: api/Registration?token = 'your token'
        //this endpoint verifies the email address
        [HttpGet]
        public RedirectResult Get([FromQuery] string token)
        {
            var email = TokenGenerator.GetUserEmail(token);
            Status status;
            if(email!=null)
            {
                status = new VShareDatabaseService().UpdateEmailVerificationStatus(email);
            }
            else
            {
                status = new Status()
                {
                    Code = 0,
                    Message = "token is invalid or has been expired"
                };
            }
           


            return Redirect(CLIENT_URL+"verifyemail?result=" +status.Code);
        }

        // POST: api/Registration
        [HttpPost]
        public Status Post([FromBody] User user_info)
        {
            var db = new VShareDatabaseService();
            Status status;
            if (!db.IsAlreadyRegistered(user_info.Email))
            {
                new VShareDatabaseService().AddUser(user_info);
                
                if(new EmailService().SendVerificationEmail(user_info.Email,API_URL))
                {
                    status = new Status()
                    {
                        Code = 1,
                        Message = "User Registered"
                    };
                }
                else
                {
                    status = new Status()
                    {
                        Code = 0,
                        Message = "Verification email not sent"
                    };
                }

            }
                
            else
                status = new Status()
                {
                    Code = 0,
                    Message = "Email is already Registered"
                };
        return status;
        }

        // PUT: api/Auth/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}
