﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Feeds;
using SocialShareApi.Models.Products;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    
    public class FeedsController : ControllerBase
    {
       
        // GET: api/Feeds
       [HttpGet]
        
        public FeedsModel Get()
        {

             String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser( accessToken);
          
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                FeedsModel accountsModel1 = new FeedsModel()
                {
                    Status = Error
                };
                return accountsModel1;

            }
            List<Feed> feeds = new VShareDatabaseService().getAllFeeds(user_id);
            Status Success = new Status()
            {
                Code = 200,
                Message = "Success"
            };
            FeedsModel feedsModel = new FeedsModel()
            {
                Feeds = feeds.ToArray(),
                Status = Success
            };

             return feedsModel;
            
        }
        [HttpGet("getProducts")]
        public List<Product> getProducts()
        {
            List<Product> products = new List<Product>();
            XDocument document = XDocument.Load("https://canadianbestseller.com/wp-content/uploads/woo-product-feed-pro/" +
                "xml/QHeiOwOvTNWW90hur1Bb8CiQKHvlsP7W.xml");
            XElement channel = document.Root.Element("channel");
            XNamespace xNamespace = XNamespace.Get("http://base.google.com/ns/1.0");

            foreach (var item in channel.Elements("item"))
            {

                var id = item.Element(xNamespace + "id").Value;
                var title = item.Element(xNamespace + "title").Value;
                var description = item.Element(xNamespace + "description").Value;
                var link = item.Element(xNamespace + "link").Value;
                var image_link = item.Element(xNamespace + "image_link").Value;
                var availability = item.Element(xNamespace + "availability").Value;
                var price = item.Element(xNamespace + "price").Value;
                var condition = item.Element(xNamespace + "condition").Value;
                Product product = new Product
                {
                    Product_id = Convert.ToInt32(id),
                    Title = title,
                    Description = description,
                    Link = link,
                    Imgurl = image_link,
                    Availability = availability,
                    Price = Regex.Match(price, @"\d+").Value,
                    Condition = condition,
                    Feed_id = 2,
                    //IsAddedToDB = false,

                };
                products.Add(product);
                if (products.Count > 10)
                    break;
            }

            foreach (var item in products)
            {
               // new VShareDatabaseService().AddProductFromFeed(item.Product_id, item.Title, item.Description, item.Imgurl, item.Link, item.Availability, item.Price, item.Condition, item.Feed_id);
               // item.IsAddedToDB = true;
            }

            return products;
        }

        // GET: api/Feeds/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

       [HttpPost]
         public Status Post([FromBody] Feed feed)
         {
             String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
             var user_id = TokenGenerator.GetUser(accessToken);
           
             if (user_id == 0)
             {
                 Status Error = new Status()
                 {
                     Code = 234,
                     Message = "UnAuthenticated Token"
                 };
                 return Error;
             }
            var db = new VShareDatabaseService();
            int id = db.AddToFeed(feed.Name, feed.Link,feed.Type, feed.Time,user_id);
             return new Status() { Code = 200, id=id,Message = "Success" };
         }

        // PUT: api/Feeds/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public Status Delete(int id)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                return Error;
            }
            new VShareDatabaseService().DeleteFeed(id, user_id);
            return new Status() { Code = 200, Message = "Success" };
        }
    }
}
