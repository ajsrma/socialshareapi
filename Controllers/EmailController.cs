﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SocialShareApi.Models;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {

        private IConfiguration _configuration;

        public string CLIENT_URL { get; }
        public string API_URL { get; set; }

        public EmailController(IConfiguration iconfig)
        {
            _configuration = iconfig;
            CLIENT_URL = _configuration.GetValue<string>("URLS:CLIENT_SIDE_URL");
            API_URL = _configuration.GetValue<string>("URLS:BASE_URL");
        }

        // GET: api/Email
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Email/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Email
        [HttpPost]
        public Status Post([FromBody] EmailRequest emailrequest)
        {
            Status status;
            if (emailrequest.Type == 1)   // 1 - verification email
            {
                if (new EmailService().SendVerificationEmail(emailrequest.Email, API_URL))
                {
                    status = new Status
                    {
                        Code = 1,
                        Message = "Verification email has been sent"
                    };
                }
                else
                {
                    status = new Status
                    {
                        Code = 0,
                        Message = "Verification email not sent"
                    };
                }
            }
            else
            {
                status = new Status
                {
                    Code = 0,
                    Message = "Verification email not sent"
                };
            }


            return status;
        }

        // PUT: api/Email/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
