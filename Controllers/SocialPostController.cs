﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialShareApi.Models;
using SocialShareApi.Models.Auth;
using SocialShareApi.Service;

namespace SocialShareApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SocialPostController : ControllerBase
    {
        // GET: api/SocialPost
        [HttpGet]
        public SocialPostsModel Get()
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                SocialPostsModel socialPostsModel1 = new SocialPostsModel()
                {
                    Status = Error
                };
                return socialPostsModel1;
            }
            List<SocialPostDetails> socialPosts = new VShareDatabaseService().getAllSocialPosts(user_id);
            SocialPostsModel socialPostsModel = new SocialPostsModel()
            {
                SocialPostDetails = socialPosts.ToArray(),
                Status = new Status
                {
                    Code = 200,
                    Message = "Success"
                }
            };
            return socialPostsModel;
        }

        // GET: api/SocialPost/5
        [HttpGet("{id}")]
        public SocialPost Get(int id)
        {
            SocialPost SocialPost= new VShareDatabaseService().GetSocialPost(id);
           
            return SocialPost;
        }

        // POST: api/SocialPost
        [HttpPost]
        public Status Post([FromBody] SocialPost socialPost )
        {
            Status status;
            try 
            {
                
                int id = new VShareDatabaseService().AddToSocialPost(socialPost.Product_Id, socialPost.Account_Id, socialPost.Post_Id,socialPost.Status,socialPost.Scheduled_Time);
                status = new Status() { Code = 200, id = id, Message = "Post addded to table" };
            }
            catch(Exception e)
            {
               status = new Status() { Code = 334, Message = "Error While Adding" };
            }
            return status;
        }

      
        


        // PUT: api/SocialPost/5
        [HttpPut("{id}")]
        public Status Put(int id, [FromBody] SocialPost socialPost )
        {
            return new VShareDatabaseService().UpdateScheduleForPost(id,socialPost.Scheduled_Time,socialPost.Status);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
