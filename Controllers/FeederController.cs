﻿using GoogleServiceAPI.Service;
using Microsoft.AspNetCore.Mvc;
using SocialShareApi.Models;
using SocialShareApi.Models.Auth;
using SocialShareApi.Models.Feeds;
using SocialShareApi.Models.Products;
using SocialShareApi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace SocialShareApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
  
    public class FeederController : ControllerBase
    {
        private const string V = "localhost";

        // GET: api/Feeder
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Feeder/5
        [HttpGet("{id}")]
        public Feeder Get(int id)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                Feeder accountsModel1 = new Feeder()
                {
                    Status = Error
                };
                return accountsModel1;
            }


            var db = new VShareDatabaseService();
            var fail = 0;
            Feed feed = db.getFeed(id,user_id);


            List<Product> products = new List<Product>();
            XDocument document = XDocument.Load(feed.Link);
            XElement channel = document.Root.Element("channel");
            XNamespace xNamespace = XNamespace.Get("http://base.google.com/ns/1.0");

            foreach (var item in channel.Elements("item"))
            {
                try
                {
                    var product_feed_id = item.Element(xNamespace + "id").Value;
                    var title = item.Element(xNamespace + "title").Value;
                    var description = item.Element(xNamespace + "description").Value;
                    var link = item.Element(xNamespace + "link").Value;
                    var image_link = item.Element(xNamespace + "image_link").Value;
                    var availability = item.Element(xNamespace + "availability").Value;
                    var price = item.Element(xNamespace + "sale_price").Value;
                    var condition = item.Element(xNamespace + "condition").Value;
                    Product product = new Product
                    {
                        Product_id = Convert.ToInt32(product_feed_id),
                        Title = title,
                        Description = description,
                        Link = link,
                        Imgurl = image_link,
                        Availability = availability,
                        Price =Regex.Match(price, @"\d+").Value,
                        Condition = condition,
                        Feed_id = feed.Feed_id,
                        //IsAddedToDB = false,

                    };

                    products.Add(product);
                }
                catch(Exception e)
                {
                    fail++;
                }
          
            }
           
            foreach (var item in products)
            {
                db.AddProductFromFeed(item.Product_id,item.Title, item.Description, item.Imgurl, item.Link, item.Availability, item.Price, item.Condition, item.Feed_id);
                //item.IsAddedToDB = true;
            }

            Feeder feeder = new Feeder()
            {
                Success = products.Count,
                Failed = fail
            };
            db.UpdateFeedFetchDetails(feed.Feed_id, products.Count, fail, DateTime.Now);
            return feeder;
        }

        // POST: api/Feeder
        [HttpPost]
        public ProductsModel Post([FromBody] ProductsModel products)
        {
            String accessToken = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //ToString().Substring(7);
            var user_id = TokenGenerator.GetUser(accessToken);
            if (user_id == 0)
            {
                Status Error = new Status()
                {
                    Code = 234,
                    Message = "UnAuthenticated Token"
                };
                ProductsModel accountsModel1 = new ProductsModel()
                {
                    Status = Error
                };
                return accountsModel1;
            }
            var fail = 0;
            var success = 0;
            var status = new Status();
            string AllproductsFail = "";
            var failedproducts = new List<Product>();

            try {
             foreach (var item in products.Products)
            {
                    item.ExceptionMessage = "None";
                    if (
                  item.Product_id != null   &&
                  item.Title != null &&
                  item.Description != null &&
                  item.Imgurl != null &&
                  item.Link != null &&
                  item.Price != null &&
                  item.Condition != null &&
                  item.Availability != null 
                  
                  
                 )
                    {

                        new VShareDatabaseService().AddProductFromFeed(item.Product_id, item.Title, item.Description, item.Imgurl, item.Link, item.Availability, item.Price, item.Condition, item.Feed_id);
                        
                        success++;
                    }
                    else {

                       
                        new VShareDatabaseService().AddFailedProduct(item.Product_id, item.Title, item.Description, item.Imgurl, item.Link, item.Availability, item.Price, item.Condition,item.Feed_id, item.ExceptionMessage);
                        
                        fail++;

                        var product_id = item.Product_id  ;
                        
                        AllproductsFail  +=   product_id + ","  ;
                        status.Message =  $"{AllproductsFail}";
                        
                    }
                    failedproducts = new VShareDatabaseService().getFailedProducts(item.Feed_id);
                }

            }
            catch(Exception ex)
            {
                fail++;
            }



            ProductsModel feeder = new ProductsModel()
            {
                Success = success,
                Failed = fail,
                Status = status,
                Products = failedproducts.ToArray()
        };
            return feeder;
        }

        // PUT: api/Feeder/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
